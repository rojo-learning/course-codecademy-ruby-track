
# What You'll Be Building
# Keeping track of all the parts of our digital lives is a pain. Now that you
# know how to write Ruby, however, you can make things easy for yourself! Let's
# start by creating a program that will keep track of our movie ratings.
#
# It'll do one of four things: add a new movie to a hash, update the rating for
# an existing movie, display the movies and ratings that are already in the
# hash, or delete a movie from the hash. If it doesn't receive one of those four
# commands, the program will output some kind of error message.

# Asks for user input to add a movie title and its rating to a given hash. If
# the movie title already exists, an error message is displayed.
#
# movies - Hash with title (key sym) - rating (value int) pair values.
#
# Examples
#
#   movies = { :"Minority Report" => 10 }
#   add(movies)
#   # => Title: Amelie
#   # => Rating: 10
#   # => The movie has been added.
#   p movies
#   # => { :"Minority Report" => 10, :"Amelie" => 10 }
#
def add(movies)
  print 'Title: '
  title  = gets.chomp.to_sym
  print 'Rating: '
  rating = gets.chomp.to_i

  if movies[title].nil?
    movies[title] = rating
    puts "The movie has been added."
  else
    puts "Ooops! This movie already exists."
  end
end

# Asks for user input to update a movie rating in a given hash. If the movie
# title does not exist, an error message is displayed.
#
# movies - Hash with title (key sym) - rating (value int) pair values.
#
# Examples
#
#   movies = { :"Minority Report" => 9 }
#   update(movies)
#   # => Title: Minority Report
#   # => New rating: 10
#   # => The movie rating was updated.
#   p movies
#   # => { :"Minority Report" => 10 }
#
def update(movies)
  print 'Title: '
  title  = gets.chomp.to_sym

  if movies[title].nil?
    puts "Ooops! That movie does't exist."
  else
    print 'New rating: '
    rating = gets.chomp.to_i
    movies[title] = rating
    puts 'The movie rating was updated.'
  end
end

# Displays the movie-rating pairs in a given hash.
#
# movies - Hash with title (key sym) - rating (value int) pair values.
#
# Examples
#
#   movies = { :"Minority Report" => 10, :"Amelie" => 10 }
#   display(movies)
#   # => Minority Report: 10
#   # => Amelie: 10
#
def display(movies)
  movies.each do | movie, rating |
    puts "#{movie}: #{rating}"
  end
end

# Asks for user input to delete a movie record in a given hash. If the movie
# title does not exist, an error message is displayed.
#
# movies - Hash with title (key sym) - rating (value int) pair values.
#
# Examples
#
#   movies = { :"Minority Report" => 10, :"Transformers" => 5 }
#   delete(movies)
#   # => Title to delete: Transformers
#   # => The movie record was deleted!
#   p movies
#   # => { :"Minority Report" => 10 }
#
def delete(movies)
  print 'Title to delete: '
  title  = gets.chomp.to_sym

  if movies[title].nil?
    puts "Ooops! That movie doesn't exist."
  else
    movies.delete(title)
    puts 'The movie record was deleted!'
  end
end

movies = {
    'Minority Report'.to_sym => 5
}

# Program/Menu loop.
loop do
  puts  'What would you like to do?'
  puts  ' - "Add" a movie.'
  puts  ' - "Update" a movie rating.'
  puts  ' - "Display" the movies info.'
  puts  ' - "Delete" a movie record.'
  puts  ' - "Exit".'
  print 'Choice: '
  choice = gets.chomp.downcase

  case choice
  when "add"     then add movies
  when 'update'  then update movies
  when 'display' then display movies
  when 'delete'  then delete movies
  when 'exit'    then break
  else puts 'ERROR: Unknown requested action.'
  end
end
