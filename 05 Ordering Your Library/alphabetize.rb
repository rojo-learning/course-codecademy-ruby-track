
# What You'll Be Building
# We noticed in the last lesson that .sort! didn't have a built-in way of
# handling sorting in reverse alphabetical order. Now that we know how to write
# our own Ruby methods, we can fix that!

# Sorts an array of strings alphabetically, ascending or descending.
#
# list    - An array of strings.
# reverse - Boolean that sets the way the list will be sorted. The default is
#           false for ascending.
#
# Examples
#
#  alphabetize!(['b', 'c', 'a'])
#  # => ['a', 'b', 'c']
#  alphabetize!(['b', 'c', 'a'], true)
#  # => ['c', 'b', 'a']
#
# Returns an alphabetically sorted version of the given array.
#
def alphabetize(list, reverse = false)
  list.sort!

  if reverse
    list.reverse!
  end

  list
end

p alphabetize(["Dad", "Mom", "Me", "Brother"])
p alphabetize(["Dad", "Mom", "Me", "Brother"], true)

