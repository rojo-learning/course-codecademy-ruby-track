
# What You'll Be Building
# This project will help you create a small program that will read a user's
# input and correct his or her capitalization. Users can provide an almost
# infinite range of input, so it makes our lives easier as programmers to make
# their input standard before doing anything with it.

print "What's your first name?"
first_name = gets.chomp.capitalize!

print "What's your last name?"
last_name = gets.chomp.capitalize!

print "In wich city do you live?"
city = gets.chomp.capitalize!

print "In wich state is your city?  (abbreviate it if possible)"
state = gets.chomp.upcase!

puts "Name: #{last_name}, #{first_name}; Location: #{city}, #{state}."
