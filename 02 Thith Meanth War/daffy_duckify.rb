
# What You'll Be Building
# Now that we can direct our program using if / else statements, we can produce
# different results based on different user input.
#
# In this project, we'll combine control flow with a few new Ruby string methods
# to Daffy Duckify a user's string, replacing each "s" with "th".

print "Hello! What do you want to do today? "
user_input = gets.chomp

# Ask for input one more time if the user just pressed «Enter» the first time.
if user_input == ""
  puts "Theems like you thaid nothing..."
  print "Again. What do you want to do today? "
  user_input = gets.chomp
end

input = user_input.downcase

# Search for «s sounds» and replace them...
# ...while doing a mess to preserve the user capitalization.
if input.include?("s") or input.include?("ce") or input.include?("ci") or
  input.include?("cy")

  if input.include?("s")
    user_input.gsub!(/s/, "th")
    user_input.gsub!(/S/, "Th")
    # I hope the user don't input a ALLCAPS word with S...
  end

  if input.include?("ce")
    user_input.gsub!(/ce/, "the")
    user_input.gsub!(/Ce/, "The")
    user_input.gsub!(/CE/, "THE")
  end

  if input.include?("ci")
    user_input.gsub!(/ci/, "thi")
    user_input.gsub!(/Ci/, "Thi")
    user_input.gsub!(/CI/, "THI")
  end

  if input.include?("cy")
    user_input.gsub!(/cy/, "thy")
    user_input.gsub!(/Cy/, "Thy")
    user_input.gsub!(/CY/, "THY")
  end

  puts "#{user_input} theemth very nice!"
else
  puts "That ith boring..."
end
