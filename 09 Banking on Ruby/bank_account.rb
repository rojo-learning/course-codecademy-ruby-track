
# What You'll Be Building
# All right! Now that you know how to control the level of privacy in a Ruby
# class, we can use that to our advantage when creating objects. In this case,
# we'll be making an Account object with public methods to display balances and
# transfer funds, but which rely on private methods to make sure the user's PIN
# (personal identification number) is correct before approving transactions.
#
# Note: We're just using banking as an example because it's a nice real-world
# analogy. This isn't for real banking, so don't use any real banking
# information!

class Account
  APPERTURE_FEE = 0

  attr_reader :name, :balance

  def initialize(name, amount)
    @name    = name
    @balance = amount
  end

  def self.apperture_fee
    @@apperture_fee
  end

  private

  def pin
    @pin = "1234"
  end

  def pin_error
    "Access denied: incorrect PIN."
  end

  def balance
    "Balance: $#{@balance}."
  end

  public

  def display_balance(pin_number)
    puts (pin_number.eql? pin) ? balance : pin_error
  end

  def withdraw(pin_number, amount)
    if pin_number.eql? pin
      if (@balance - amount) >= 0
        @balance -= amount
        puts "Withdrew $#{amount}."
        display_balance(pin_number)
      else
        puts "Non-sufficient funds."
      end
    else
      puts pin_error
    end
  end

  def deposit(pin_number, amount)
    if pin_number.eql? pin
      @balance += amount
      puts "Deposited $#{amount}."
      display_balance(pin_number)
    else
      puts pin_error
    end
  end
end

class CheckingAccount < Account
  APPERTURE_FEE = 500
end

class SavingsAccount < Account
end

def create_account(holder, amount, type)
  if amount >= type::APPERTURE_FEE
    puts "A new #{type.ancestors[0]} was created."
    type.new(holder, amount)
  else
    puts "Non-sufficient funds to open the #{type.ancestors[0]}."
  end
end

my_account = create_account("David Rojo", 100, CheckingAccount)
my_account = create_account("David Rojo", 100, SavingsAccount)

my_account.display_balance('123')
my_account.display_balance('1234')
my_account.deposit('1234', 100)
my_account.withdraw('1234', 300)
my_account.withdraw('1234', 200)
