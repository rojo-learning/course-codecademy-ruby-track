
# What You'll Be Building
# Hiding information is a major part of programming: protecting passwords,
# establishing secure connections, and securing programs against tampering all
# rely on controlling access to information.
#
# While we won't be able to really dig into information hiding until after we
# cover hashes in a later course, we can write a simple program to change a
# user's input with the tools we have now: arrays and iterators.

# Get text to be redacted.
puts "Enter some text with that includes non polite words: "
text = gets.chomp

# Get the list of unwanted words.
puts "Add a non polite words to be redacted, separated by spaces: "
unpolite_words = gets.chomp.downcase.split

# Holder for the redacted version.
redacted_text = ""

# Split the text and redact unwanted words in each fragment.
# This version works for text with punctuation, but it may not work well for
# text with dash separated words: up-to, down-to, back-end.
text.split.each do | fragment |
  redacted = false

  unpolite_words.each do | word |
    if fragment.downcase.include? word
      redacted_text += fragment.downcase.gsub(word, "REDACTED")
      redacted = true
      break
    end
  end

  if not redacted
    redacted_text += fragment
  end

  redacted_text += " "
end

redacted_text.chomp!

puts text
puts redacted_text
