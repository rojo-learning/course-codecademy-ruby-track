
# What You'll Be Fixing
# All right! We're going to reinforce our knowledge of Ruby best practices by
# refactoring some existing code. As mentioned, refactoring is the process by
# which we improve a code's structure, appearance, and/or performance without
# modifying its overall behavior.
#
# The code in the editor is a Ruby method, first_n_primes, that takes a number n
# and generates a list of the first n prime numbers. Unfortunately, our poor
# author hasn't yet mastered all the tools available in Ruby. But we can fix
# that!

# Original version

$VERBOSE = nil    # No compiler coplains if using Ruby 1.9+
require 'prime'   # This is a module.

def first_n_primes(n)

  unless n.is_a? Integer
    return "n must be an integer."
  end

  if n <= 0
    return "n must be greater than 0."
  end

  prime_array = [] if prime_array.nil?

  prime = Prime.new
  for num in (1..n)
    prime_array.push(prime.next)
  end
  return prime_array
end

# Refactored version for Ruby 1.8

def ref_first_n_primes(n)
  return "n must be an integer." unless n.is_a? Integer
  return "n must be greater than 0." if n <= 0

  prime_array ||= []

  prime = Prime.new
  n.times { prime_array << prime.next }

  prime_array
end


# Example version for Ruby 1.9

def aut_first_n_primes(n)
  # Check for correct input!
  "n must be an integer" unless n.is_a? Integer
  "n must be greater than 0" if n <= 0

  # The Ruby 1.9 Prime class makes the array automatically!
  prime = Prime.instance
  prime.first n
end

first_n_primes(10)
ref_first_n_primes(10)
aut_first_n_primes(10)
